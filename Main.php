<!DOCTYPE html>
<html>
	<head>
		<title>Tugas Akhir Pemrograman Web</title>
		<link rel="stylesheet" type="text/css" href="styleUK.css">
	</head>
	<body>

		<!-- bagian header -->
		<header class="warna">
			<div class="container">
				<h1><a href="">TSAN FAROSSI MA ALI</a></h1>
				<ul>
					<li><a href="#about">About</a></li>
					<li><a href="#portfolio">Portfolio</a></li>
					<li><a href="#fitur">Fitur</a></li>
					<li><a href="#pr">SiswaPrestasi</a></li>
					<li><a href="#contact">Contact</a></li>
					<li><a href="Saran.php">Kritik/Saran</a></li>
				</ul>
				

			</div>
		</header>

		<!-- bagian banner -->
		<section class="banner">
			<div class="container">
				<div class="banner-left">
					<h2>Hai...<br>
						Saya adalah seorang <span class="efek-ngetik"></span></h2>
					<p>Selamat datang di website Tugas Akhir saya </p>
				</div>
			</div>
		</section>
		<main>
		<!-- bagian about -->
		<section id="about">
			<div class="container">
				<h3>About</h3>
				&nbsp;&nbsp;&nbsp;<P>Assalamualaikum wr.wb.
					Namaku Tsan Farossi Ma ali, lahir di Ngawi, 02 Agustus 2001. Aku adalah anak kedua dari dua bersaudara. Tsan adalah 
					panggilanku. Aku terlahir dari keluarga yang sangat sederhana. Sejak kecil ayahku selalu menasehatiku agar rajin bertanggung jawab, bersikap jujur, dan 
					baik terhadap sesama. Aku mulai bersekolah di SDN Karangjati 1, kemudian 
					setelah lulus melanjutkan di SMP N 1 Karangjati, Setelah itu aku melanjutkan sekolah di SMA N 2 Ngawi. Sekarang ini aku melanjutkan pendidikan di Universitas Ahmad Dahlan 
					mengambil jurusan Teknik Informatika.</p>
			</div>
		</section>


		<section id="pr">
			<div class="container">
				<h3>Siswa Berprestasi</h3>
		<?php 
		$arrNama = array ("SHAFLY ZUHERNATA","LAUDIA BINTANG ARTUTA"); 

		echo "<h1>Nama-nama Siswa Berprestasi Angkatan 2020 : </h1><br>"; 
		for ($i=0; $i<count($arrNama); $i++) { 
			echo $arrNama[$i] ."<br>"; 
		} 
		
		?> 
		</div>
		</section>

		<!-- bagian contact -->
		<section id="kontak">
			<div class="container">
				<h3>Contact</h3>
				<div class="col-4">
					<h4>Alamat</h4>
					<p>Karangjati RT 01, Kec.Karangkati, Ngawi, Jawa Timur.</p>
				</div>

				<div class="col-4">
					<h4>Email</h4>
					<p>tsanfarossim@gmail.com</p>
				</div>

				<div class="col-4">
					<h4>Telp/Hp</h4>
					<p>081330973017</p>
				</div>	

                <div class="col-4">
					<h4>Media Sosial</h4>
                    <p> <a href="https://www.instagram.com/tsan.farossi/">INSTAGRAM</a></p>
				</div>	
                
			</div>
		</section>
	</main>	

		<!-- bagian footer -->
		<footer>
			<div class="container">
				<small>Dibuat &copy; 2021 - Tsan Farossi.</small>
			</div>
		</footer>

		<script src="text.js"></script>
	</body>
</html>