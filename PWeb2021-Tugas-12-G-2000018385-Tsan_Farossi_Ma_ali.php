<?php
    // membuat array kosong
    $buah = array();
    $hobi = [];

    // membuat array sekaligus mengisinya
    $minuman = array("Kopi", "Teh", "Jus Jeruk");
    $barang = ["Modem", "Hardisk", "Flashdisk"];

    // membuat array dengan mengisi indeks tertentu
    $makanan[1] = "Soto";
    $makanan[2] = 18.29;
    $makanan[0] = "Nasi Goreng";

    // menampilkan isi array
    echo $makanan[0]."<br>";
    echo $makanan[1]."<br>";
    echo $makanan[2]."<br><br>";

    // menampilkan isi array dengan perulangan for
    for($i=0; $i < count($barang); $i++){
        echo $barang[$i]."<br>";
    } echo "<br>";

    // menampilkan isi array dengan perulangan foreach
    foreach($minuman as $isi){
        echo $isi."<br>";
    } echo "<hr>";
?>